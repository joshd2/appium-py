"""
Regression Test that will verify the number of docs currently uploaded,
capture a new picture, submit the new doc and verify the number of
docs uploaded.
"""
from time import sleep
from helpers.actions import Actions
from helpers.xpaths import xpath


# LOGIN
def test_click_sign_in():
    """
    Taps Sign In button.
    """
    Actions.click_id("com.simplenexus.loans.client:id/secondaryBtn")
    sleep(2)


def test_send_email():
    """
    Sends email to text field.
    """
    Actions.send_text_by_xpath(xpath["email"], "davisbor@appium.com")


def test_send_password():
    """
    Sends password to text field.
    """
    Actions.send_text_by_xpath(xpath["password"], "Madrigal2012")


def test_login():
    """
    Logs into the app with the given credentials.
    """
    Actions.click_id("com.simplenexus.loans.client:id/primaryActionButton")
    sleep(5)


# VERIFY DATA BEFORE CHANGES
def test_click_docs_first():
    """
    Taps on Docs bottom bar button.
    """
    Actions.click_id("com.simplenexus.loans.client:id/docPortalBtnTab")
    sleep(3)


def test_click_my_uploads_first():
    """
    Taps on My Uploads.
    """
    Actions.click_xpath(xpath["uploads"])
    sleep(3)


def test_verify_original_docs_num():
    """
    Gets the number of docs uploaded from the toolbar text and taps back.
    """
    global original_num
    upload_text = Actions.get_text("com.simplenexus.loans.client:id/toolbar_title")
    original_num = int(''.join(list(filter(str.isdigit, upload_text))))
    Actions.click_id("com.simplenexus.loans.client:id/goBackButton")
    sleep(3)


# TEST START
def test_click_my_loan_tab():
    Actions.click_id("com.simplenexus.loans.client:id/myloanBtnTab")
    sleep(3)


def test_click_add_document_icon():
    Actions.click_id("com.simplenexus.loans.client:id/small_button_icon")
    sleep(3)


def test_click_take_picture():
    Actions.click_id("com.simplenexus.loans.client:id/take_picture")
    sleep(3)


def test_click_ok():
    Actions.click_id("android:id/button1")
    sleep(3)


def test_click_onboarding_done():
    # TODO: Add while loop with is_displayed
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    sleep(3)

# def test_allow_pictures():
#     Actions.click_id("com.android.permissioncontroller:id/permission_allow_foreground_only_button")


def test_capture_picture():
    Actions.click_id("com.simplenexus.loans.client:id/scan_capture_button")
    sleep(3)


def test_click_next():
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    sleep(3)


def test_click_crop():
    Actions.click_id("com.simplenexus.loans.client:id/scanner_btn_next")
    sleep(3)


def test_click_got_it():
    Actions.click_id("com.simplenexus.loans.client:id/btnDone")
    sleep(3)


def test_click_use_image_done():
    Actions.click_id("com.simplenexus.loans.client:id/adjust_done")
    sleep(3)


def test_click_review():
    Actions.click_id("com.simplenexus.loans.client:id/send_doc")
    sleep(3)


def test_click_submit():
    Actions.click_id("com.simplenexus.loans.client:id/submit_button")
    sleep(5)


# VERIFY UPLOADED DOCUMENT
def test_click_docs():
    Actions.click_id("com.simplenexus.loans.client:id/docPortalBtnTab")
    sleep(3)


def test_click_my_uploads():
    Actions.click_xpath(xpath["uploads"])
    sleep(3)


def test_get_upload_toolbar_text():
    global new_num
    sleep(2)
    upload_new_text = Actions.get_text("com.simplenexus.loans.client:id/toolbar_title")
    new_num = int(''.join(list(filter(str.isdigit, upload_new_text))))
    assert original_num + 1 is new_num, "New Upload number is not correct"
    Actions.click_id("com.simplenexus.loans.client:id/goBackButton")
