from selenium import webdriver

caps = {
  "platformName": "android",
  "appium:deviceName": "Pixel",
  "appium:automationName": "UiAutomator2",
  "adbExecTimeout": "100000"
}

driver = webdriver.Remote("http://0.0.0.0:4723", caps)
