from config import driver


class Actions:
    def click_id(element):
        el = driver.find_element_by_id(element)
        return el.click()

    def click_xpath(element):
        el = driver.find_element_by_xpath(element)
        return el.click()

    def get_text(element):
        el = driver.find_element_by_id(element)
        return el.text

    def is_displayed(element):
        el = driver.find_element_by_id(element)
        return True if el.is_displayed else False

    def send_text_by_xpath(element, text):
        el = driver.find_element_by_xpath(element)
        return el.send_keys(text)
